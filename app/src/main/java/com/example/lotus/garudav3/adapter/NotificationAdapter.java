package com.example.lotus.garudav3.adapter;

import android.app.Notification;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.lotus.garudav3.R;
import com.example.lotus.garudav3.model.Notifications;

import java.util.List;

/**
 * Created by Lotus on 16/01/2018.
 */

public class NotificationAdapter extends RecyclerView.Adapter<NotificationAdapter.MyViewHolder> {

    private List<Notifications> notifList;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView title, subtitle;

        public MyViewHolder(View view) {
            super(view);
            title = (TextView) view.findViewById(R.id.title);
            subtitle = (TextView) view.findViewById(R.id.subtitle);
        }
    }


    public NotificationAdapter(List<Notifications> notifList) {
        this.notifList = notifList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.activity_notification_list, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Notifications notif = notifList.get(position);
        holder.title.setText(notif.getTitle());
        holder.subtitle.setText(notif.getSubtitle());
    }

    @Override
    public int getItemCount() {
        return notifList.size();
    }
}
