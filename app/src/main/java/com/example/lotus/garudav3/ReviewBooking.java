package com.example.lotus.garudav3;

import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * Created by Lotus on 20/11/2017.
 */

public class ReviewBooking extends AppCompatActivity {

    Toolbar toolbar;
    String arrivalCode;
    int totalPax;
    int totalTax;
    int totalTaxDua;
    String priceInt;
    String priceIntDua;

    TextView departureTime;
    TextView boardingDate;
    TextView flightTime;
    TextView baggage;
    TextView baggageDown;
    TextView boardingTime;
    TextView departurePort;
    TextView arrival;
    TextView passengerTotal;
    TextView priceTotal;
    TextView priceStr;
    TextView addedTax;
    TextView priceDua;
    TextView arrivalTime;
    TextView flightTimeKanan;
    TextView namaBawah;
    TextView namaAtas;
    TextView summaryBook;
    TextView summaryBookBawah;

    String departureDateStr;
    String totalPaxStr;
    String totalPaxStrBawah;
    String departureCode;
    String minuteBoarding;
    String hourBoarding;
    Date d;

    String arrivalStationTerminal;
    String departureStationTerminal;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_review_booking);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setElevation(0);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        departurePort = (TextView)findViewById(R.id.departure_port);
        departureTime = (TextView) findViewById(R.id.departure_time);
        boardingDate = (TextView) findViewById(R.id.boarding_date_r);
        baggage = (TextView) findViewById(R.id.baggage);
        baggageDown = (TextView) findViewById(R.id.baggage_down);
        arrival = (TextView)findViewById(R.id.arrival_port);
        passengerTotal = (TextView) findViewById(R.id.passenger_name);
        priceStr = (TextView)findViewById(R.id.price);
        priceTotal = (TextView)findViewById(R.id.price_total);
        flightTime = (TextView) findViewById(R.id.flight_time);
        boardingTime = (TextView) findViewById(R.id.boarding_time);
        addedTax = (TextView) findViewById(R.id.added_tax);
        priceDua = (TextView) findViewById(R.id.price_dua);
        arrivalTime = (TextView) findViewById(R.id.arrival_time);
        flightTimeKanan = (TextView) findViewById(R.id.flight_time_kanan);
        namaBawah = (TextView) findViewById(R.id.nama_garuda_bawah);
        namaAtas = (TextView) findViewById(R.id.nama_garuda_atas);
        summaryBook = (TextView) findViewById(R.id.summary_book);
        summaryBookBawah = (TextView) findViewById(R.id.summary_book_bawah);

        Intent i = getIntent();
        departureCode = i.getStringExtra("departureCode");
        arrivalCode = i.getStringExtra("arrivalCode");
        totalPax = i.getIntExtra("totalPax",0);
        departureDateStr = i.getStringExtra("departureDate");
        String arrivalDateStr = i.getStringExtra("arrivalDate");
        final String price = i.getStringExtra("price");
        final String tanggalKanan = i.getStringExtra("priceDua");
        totalPaxStr = i.getStringExtra("totalPaxStr");
        totalPaxStrBawah = i.getStringExtra("totalPaxStrBawah");
        priceInt = i.getStringExtra("priceInt");
        priceIntDua = i.getStringExtra("priceIntDua");
        totalTax = i.getIntExtra("totalTax", 0);
        totalTaxDua = i.getIntExtra("totalTaxDua", 0);

        departureStationTerminal = i.getStringExtra("departureStationTerminal");
        arrivalStationTerminal = i.getStringExtra("arrivalStationTerminal");
        TextView departureTerminal = (TextView)findViewById(R.id.departure);
        TextView arrivalTerminal = (TextView)findViewById(R.id.arrival);
        departureTerminal.setText("T. "+departureStationTerminal);
        arrivalTerminal.setText("T. "+arrivalStationTerminal);

        //String hargaTampil = NumberFormat.getIntegerInstance(Locale.GERMAN).format(Integer.valueOf(priceInt));
        int hargaTampil = (Integer.valueOf(priceInt)* totalPax) + (totalTax * totalPax)+ 303 + (Integer.valueOf(priceIntDua)* totalPax);

        departurePort.setText(departureCode);
        departureTime.setText(departureDateStr);
        namaBawah.setText(arrivalCode + " - " + departureCode);
        namaAtas.setText(departureCode + " - " + arrivalCode);
        arrival.setText(arrivalCode);
        arrivalTime.setText(arrivalDateStr);
        summaryBook.setText(totalPaxStr);
        summaryBookBawah.setText(totalPaxStrBawah);
        passengerTotal.setText(totalPax + " person(s)");
        flightTime.setText(price.substring(0,5).replace(".",":"));
        flightTimeKanan.setText(tanggalKanan.substring(0,5).replace(".",":"));
        baggage.setText("Baggage " + departureCode + " - " + arrivalCode);
        baggageDown.setText("Baggage " + arrivalCode + " - " + departureCode);
        priceStr.setText("Rp " + NumberFormat.getIntegerInstance(Locale.GERMAN).format(Integer.valueOf(priceInt) * totalPax));
        priceDua.setText("Rp " + NumberFormat.getIntegerInstance(Locale.GERMAN).format(Integer.valueOf(priceIntDua) * totalPax));
        addedTax.setText("Rp " +  NumberFormat.getIntegerInstance(Locale.GERMAN).format(totalTax * totalPax));
        priceTotal.setText("Rp " + NumberFormat.getIntegerInstance(Locale.GERMAN).format(hargaTampil)); //+ new Random().nextInt(899) + 100);

        try {
            SimpleDateFormat sdf = new SimpleDateFormat("hh:mm:ss");
            d = sdf.parse(price.substring(0,5).replace(".",":")+":00");
            long time = d.getTime();
            sdf.applyPattern("HH:mm");
            boardingTime.setText(sdf.format(time-3600000));
            minuteBoarding = sdf.format(time-3600000).toString().split(":")[1];
            hourBoarding = sdf.format(time-3600000).toString().split(":")[0];
        } catch (ParseException ex) {
            Log.d("TAG", ex.toString());
        }
        String boardingDateStr = departureDateStr.substring(departureDateStr.lastIndexOf(",") + 2);
        boardingDate.setText(boardingDateStr);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        /*int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }*/
        if (item.getItemId() == android.R.id.home)
        {
            finish();
        }

        return super.onOptionsItemSelected(item);
    }
}