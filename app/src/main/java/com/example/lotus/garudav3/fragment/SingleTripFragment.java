package com.example.lotus.garudav3.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.lotus.garudav3.BookActivity;
import com.example.lotus.garudav3.BookDetailActivitySingle;
import com.example.lotus.garudav3.R;
import com.example.lotus.garudav3.SearchDestinationActivity;
import com.example.lotus.garudav3.model.Destination;
import com.skyfishjy.library.RippleBackground;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Lotus on 15/11/2017
 */

public class SingleTripFragment extends android.support.v4.app.Fragment{

    TextView searchTravel;
    ImageView searchDestination;
    Intent intent;
    int REQUEST_CODE_DEPARTURE = 11;
    int REQUEST_CODE = 99;
    String arrivalCode = "CGK";
    String departureCode = "CGK";
    String dateFromWeb = "";
    RippleBackground rippleBackground;

    RelativeLayout departureFrom;

    TextView leaveFromCity;
    TextView leaveFromAirport;
    TextView leaveFromCode;
    ImageView logoImage;

    ImageView searchIcon;

    int destinationSelected;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rel = inflater.inflate(R.layout.fragment_singletrip, container, false);

        destinationSelected = 0;

        leaveFromCity = rel.findViewById(R.id.leave_from_city);
        leaveFromAirport = rel.findViewById(R.id.leave_from_airport);
        leaveFromCode = rel.findViewById(R.id.leave_from_code);
        logoImage = rel.findViewById(R.id.img_logo);

        View.OnClickListener searchDestinationClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent = new Intent(getContext(), SearchDestinationActivity.class);
                String title = "Destination List";
                intent.putExtra("title",title);
                startActivityForResult(intent, REQUEST_CODE);
            }
        };

        searchTravel = rel.findViewById(R.id.search_travel);
        searchTravel.setOnClickListener(searchDestinationClickListener);
        searchIcon = rel.findViewById(R.id.search_icon_single);
        searchIcon.setOnClickListener(searchDestinationClickListener);

        rippleBackground=(RippleBackground)rel.findViewById(R.id.content);
        departureFrom = rel.findViewById(R.id.departure);

        departureFrom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent = new Intent(getContext(), SearchDestinationActivity.class);
                String title = "Origin List";
                intent.putExtra("title",title);
                startActivityForResult(intent, REQUEST_CODE_DEPARTURE);
            }
        });

        searchDestination = rel.findViewById(R.id.search_destination);
        searchDestination.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                intent = new Intent(getContext(), BookDetailActivitySingle.class);
                intent.putExtra("arrivalCode",arrivalCode);
                intent.putExtra("departureCode",departureCode);
                if(!dateFromWeb.equals(""))
                    intent.putExtra("dateFromWeb",dateFromWeb);
                startActivity(intent);
                rippleBackground.stopRippleAnimation();
            }
        });

        dateFromWeb = ((BookActivity) getActivity()).date;
        arrivalCode = ((BookActivity) getActivity()).departure;
        departureCode = ((BookActivity) getActivity()).origin;

        Log.d("json belum masuk", dateFromWeb + " " + arrivalCode + " " + departureCode);

        if(!dateFromWeb.equals("") && !arrivalCode.equals("") && !departureCode.equals("")
                && !dateFromWeb.equals("null") && !arrivalCode.equals("null") && !departureCode.equals("null")) {
            Log.d("json masuk", dateFromWeb + " " + arrivalCode + " " + departureCode);
            getFromWeb(arrivalCode, departureCode);
            searchIcon.setVisibility(View.GONE);
            searchDestination.setVisibility(View.VISIBLE);
            rippleBackground.startRippleAnimation();
        }

        return rel;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == -1) {
            if(requestCode == REQUEST_CODE){
                destinationSelected = 1;
                // Jika kembali dengan menekan list item
                String city = data.getStringExtra("city");
                String airport = data.getStringExtra("airport");
                int logo = data.getIntExtra("logo", R.drawable.jakarta);
                String code = airport.substring(0, airport.indexOf(" "));
                searchTravel.setText(city);
                arrivalCode = code;
            }else if(requestCode == REQUEST_CODE_DEPARTURE){
                // Jika kembali dengan menekan list item
                String city = data.getStringExtra("city");
                String airport = data.getStringExtra("airport");
                int logo = data.getIntExtra("logo", R.drawable.jakarta);
                String code = airport.substring(0, airport.indexOf(" "));
                String cityOnly = city.substring(0,city.indexOf(","));
                String airportOnly = airport.substring(airport.indexOf(" - ") + 3,airport.length());
                departureCode = code;
                leaveFromCity.setText(cityOnly);
                leaveFromAirport.setText(airportOnly);
                leaveFromCode.setText(departureCode);
                logoImage.setImageResource(logo);
            }
            if(destinationSelected == 1){
                searchIcon.setVisibility(View.GONE);
                searchDestination.setVisibility(View.VISIBLE);
                rippleBackground.startRippleAnimation();
            }
        }
    }

    public void getFromWeb(String departureCode, String portCode){
        Log.d("json", "get from web");
        List<Destination> destinationList = new ArrayList<>();
        destinationList.add(new Destination("Jakarta, Indonesia","CGK - Soekarno Hatta", R.drawable.jakarta));
        destinationList.add(new Destination("Surabaya, Indonesia","SUB - Juanda", R.drawable.surabaya));
        destinationList.add(new Destination("Bali, Indonesia","DPS - Ngurah Rai Int'l", R.drawable.bali));
        destinationList.add(new Destination("Lombok, Indonesia","LOP - Lombok Int'l Airport", R.drawable.lombok));
        destinationList.add(new Destination("Yogyakarta, Indonesia","JOG - Adi Sutjipto", R.drawable.jogjakarta));
        destinationList.add(new Destination("Medan, Indonesia","KNO - Kuala Namu", R.drawable.medan));
        destinationList.add(new Destination("Jayapura, Indonesia","DJJ - Sentani", R.drawable.jayapura));

        for(int i = 0; i<destinationList.size(); i++)
        {
            if(destinationList.get(i).getAirport().substring(0,3).equals(portCode))
            {
                Log.d("json", "in portcode");
                leaveFromCity.setText(destinationList.get(i).getCity().split(",")[0]);
                leaveFromAirport.setText(destinationList.get(i).getAirport().split(" - ")[1]);
                leaveFromCode.setText(portCode);
                logoImage.setImageResource(destinationList.get(i).getLogo());
            }
            if(destinationList.get(i).getAirport().substring(0,3).equals(departureCode))
            {
                Log.d("json", "in departurecode");
                searchTravel.setText(destinationList.get(i).getCity());
            }
        }
    }
}
