package com.example.lotus.garudav3.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.lotus.garudav3.R;
import com.example.lotus.garudav3.model.Route;

import java.util.List;

public class RouteAdapter extends RecyclerView.Adapter<RouteAdapter.MyViewHolder> {

    private List<Route> routeList;

    class MyViewHolder extends RecyclerView.ViewHolder {
        TextView priceReal, name, price, time, duration, method, point;

        MyViewHolder(View view) {
            super(view);
            priceReal = view.findViewById(R.id.price_real);
            name = view.findViewById(R.id.name);
            price = view.findViewById(R.id.price);
            time = view.findViewById(R.id.time);
            duration = view.findViewById(R.id.duration);
            //method = view.findViewById(R.id.method);
            point = view.findViewById(R.id.point);
        }
    }

    public RouteAdapter(List<Route> routeList) {
        this.routeList = routeList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.route_list, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Route route = routeList.get(position);
        holder.priceReal.setText(route.getPriceReal());
        holder.name.setText(route.getName());
        holder.price.setText(route.getPrice());
        holder.time.setText(route.getTime());
        holder.duration.setText(route.getDuration());
        //holder.method.setText(route.getMethod());
        holder.point.setText(route.getPoints());
    }

    @Override
    public int getItemCount() {
        return routeList.size();
    }
}