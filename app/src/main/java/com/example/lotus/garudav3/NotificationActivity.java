package com.example.lotus.garudav3;

import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.Intent;
import android.media.Image;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.lotus.garudav3.adapter.DestinationAdapter;
import com.example.lotus.garudav3.adapter.NotificationAdapter;
import com.example.lotus.garudav3.model.Destination;
import com.example.lotus.garudav3.model.Notifications;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Lotus on 16/01/2018.
 */

public class NotificationActivity extends AppCompatActivity {

    RecyclerView recyclerView;
    List<Notifications> notificationList = new ArrayList<>();
    private Toolbar mToolbar;
    NotificationAdapter adapter;
    private NotificationAdapter mAdapter;
    ImageButton renew;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);
        mToolbar = (Toolbar) findViewById(R.id.toolbar);

        setSupportActionBar(mToolbar);
        getSupportActionBar().setElevation(0);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        recyclerView = (RecyclerView) findViewById(R.id.notification_list);
        mAdapter = new NotificationAdapter(notificationList);
        renew = (ImageButton) findViewById(R.id.renew);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.addItemDecoration(new DividerItem(this, LinearLayoutManager.VERTICAL, 16));

        renew.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

        Intent i = getIntent();
        if(i.getStringExtra("message") != null) {
            String message = i.getStringExtra("message");
            String subMessage = i.getStringExtra("subtitle");

            notificationList.add(new Notifications(message, subMessage));
        }

        notificationList.add(new Notifications("Best Deals with Your Family!", "Celebrate the Festivity of Chinese New Year with our special deals"));
        notificationList.add(new Notifications("Explore the Beauty of Sulawesi", "Enjoy our new routes to Sulawesi"));
        notificationList.add(new Notifications("Wonderful Indonesia Travel Pass", "Enjoy our exclusive offer by saving up to 30% "));
        notificationList.add(new Notifications("Interested in an Upgrade?", "Use our innovative BidUpgrade request system"));
        notificationList.add(new Notifications("Best Hotel Offer!", "Find best hotel offers in your favorite destination now!"));
        notificationList.add(new Notifications("Online Promo", "Enjoy exclusive online deals from Garuda Indonesia!"));
        notificationList.add(new Notifications("Use Your E-Ticket", "Use e-ticket to simplify your travel experience"));
        notificationList.add(new Notifications("Discount up to 20%", "Plan your 2018 new year's eve holiday!"));
        notificationList.add(new Notifications("Welcome!", "Welcome in Garuda Apps"));

        adapter = new NotificationAdapter(notificationList);
        recyclerView.setAdapter(adapter);


        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(NotificationActivity.this, recyclerView, new ClickListener() {
            @Override
            public void onClick(View view, final int position) {
                Log.d("clicked", "clicked");
            }

            @Override
            public void onLongClick(View view, int position) {
                Toast.makeText(NotificationActivity.this, "Long press on position :"+view.getId(), Toast.LENGTH_LONG).show();
            }
        }));
    }

    public static interface ClickListener{
        public void onClick(View view,int position);
        public void onLongClick(View view,int position);
    }

    class RecyclerTouchListener implements RecyclerView.OnItemTouchListener{

        private ClickListener clicklistener;
        private GestureDetector gestureDetector;

        public RecyclerTouchListener(Context context, final RecyclerView recycleView, final ClickListener clicklistener){

            this.clicklistener=clicklistener;
            gestureDetector=new GestureDetector(context,new GestureDetector.SimpleOnGestureListener(){
                @Override
                public boolean onSingleTapUp(MotionEvent e) {
                    return true;
                }

                @Override
                public void onLongPress(MotionEvent e) {
                    View child=recycleView.findChildViewUnder(e.getX(),e.getY());
                    if(child!=null && clicklistener!=null){
                        clicklistener.onLongClick(child,recycleView.getChildAdapterPosition(child));
                    }
                }
            });
        }

        @Override
        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
            View child=rv.findChildViewUnder(e.getX(),e.getY());
            if(child!=null && clicklistener!=null && gestureDetector.onTouchEvent(e)){
                clicklistener.onClick(child,rv.getChildAdapterPosition(child));
            }

            return false;
        }

        @Override
        public void onTouchEvent(RecyclerView rv, MotionEvent e) {

        }

        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        /*int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }*/
        if (item.getItemId() == android.R.id.home)
        {
            finish();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        Intent i = new Intent(NotificationActivity.this, MainActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(i);
        finish();
    }

}

