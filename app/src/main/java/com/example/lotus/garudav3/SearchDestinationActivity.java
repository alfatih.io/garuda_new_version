package com.example.lotus.garudav3;

import android.content.Context;
import android.content.Intent;
import android.graphics.ColorSpace;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.lotus.garudav3.adapter.DestinationAdapter;
import com.example.lotus.garudav3.model.Destination;

import java.util.ArrayList;
import java.util.List;

public class SearchDestinationActivity extends AppCompatActivity {

    RecyclerView recyclerView;
    List<Destination> destinationList = new ArrayList<>();
    private Toolbar mToolbar;
    EditText inputSearch;
    DestinationAdapter adapter;
    RecyclerView.LayoutManager mLayoutManager;
    List<Destination> filteredList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_destination);
        mToolbar = (Toolbar) findViewById(R.id.toolbar);

        setSupportActionBar(mToolbar);
        getSupportActionBar().setElevation(0);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        inputSearch = (EditText) findViewById (R.id.title_actionbar);

        Intent intent = getIntent();
        String title = intent.getStringExtra("title");

        recyclerView = (RecyclerView) findViewById(R.id.destination_list);
        mLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.addItemDecoration(new DividerItem(this, LinearLayoutManager.VERTICAL, 16));
        //TextView titleActionBar = (TextView)findViewById(R.id.title_actionbar);
        //titleActionBar.setText(title);

        destinationList.add(new Destination("Jakarta, Indonesia","CGK - Soekarno Hatta", R.drawable.jakarta));
        destinationList.add(new Destination("Surabaya, Indonesia","SUB - Juanda", R.drawable.surabaya));
        destinationList.add(new Destination("Bali, Indonesia","DPS - Ngurah Rai Int'l", R.drawable.bali));
        destinationList.add(new Destination("Lombok, Indonesia","LOP - Lombok Int'l Airport", R.drawable.lombok));
        destinationList.add(new Destination("Yogyakarta, Indonesia","JOG - Adi Sutjipto", R.drawable.jogjakarta));
        destinationList.add(new Destination("Medan, Indonesia","KNO - Kuala Namu", R.drawable.medan));
        destinationList.add(new Destination("Jayapura, Indonesia","DJJ - Sentani", R.drawable.jayapura));
        //destinationList.add(new Destination("Solo, Indonesia","SOC - Adi Sumarmo"));
        //destinationList.add(new Destination("Pangkal Pinang, Indonesia","PGK - Depati Amir"));
        //destinationList.add(new Destination("Singapore, Singapore","SIN - Changi Intl"));

        filteredList = new ArrayList<>(destinationList);

        adapter = new DestinationAdapter(destinationList);
        recyclerView.setAdapter(adapter);
        addTextListener();


        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(this, recyclerView, new ClickListener() {
            @Override
            public void onClick(View view, final int position) {
                Intent intent = getIntent();
                intent.putExtra("city", filteredList.get(position).getCity());
                intent.putExtra("airport", filteredList.get(position).getAirport());
                intent.putExtra("logo", filteredList.get(position).getLogo());
                setResult(RESULT_OK,intent);
                finish();
            }

            @Override
            public void onLongClick(View view, int position) {
                Toast.makeText(SearchDestinationActivity.this, "Long press on position :"+view.getId(), Toast.LENGTH_LONG).show();
            }
        }));
    }

    public void addTextListener(){
        inputSearch.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s) {}
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            public void onTextChanged(CharSequence query, int start, int before, int count) {
                String queryString = query.toString().toLowerCase();
                Log.d("cobaCharSebelum ", query.toString());

                filteredList = new ArrayList<>();

                if (queryString.isEmpty()) {
                    filteredList = destinationList;
                }
                else {
                    for (Destination row : destinationList) {
                        Log.d("cobaCharSebelum", row.getCity().toString());
                        if (row.getCity().toLowerCase().split(", ")[0].contains(queryString)){; //|| row.getAirport().contains(charSequence)) {
                            Log.d("cobaCharSetelah ", queryString);
                            filteredList.add(row);
                        }
                    }
                }

                recyclerView.setLayoutManager(new LinearLayoutManager(SearchDestinationActivity.this));
                adapter = new DestinationAdapter(filteredList);
                recyclerView.setAdapter(adapter);
                adapter.notifyDataSetChanged();  // data set changed
            }
        });
    }

    public static interface ClickListener{
        public void onClick(View view,int position);
        public void onLongClick(View view,int position);
    }

    class RecyclerTouchListener implements RecyclerView.OnItemTouchListener{

        private ClickListener clicklistener;
        private GestureDetector gestureDetector;

        public RecyclerTouchListener(Context context, final RecyclerView recycleView, final ClickListener clicklistener){

            this.clicklistener=clicklistener;
            gestureDetector=new GestureDetector(context,new GestureDetector.SimpleOnGestureListener(){
                @Override
                public boolean onSingleTapUp(MotionEvent e) {
                    return true;
                }

                @Override
                public void onLongPress(MotionEvent e) {
                    View child=recycleView.findChildViewUnder(e.getX(),e.getY());
                    if(child!=null && clicklistener!=null){
                        clicklistener.onLongClick(child,recycleView.getChildAdapterPosition(child));
                    }
                }
            });
        }

        @Override
        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
            View child=rv.findChildViewUnder(e.getX(),e.getY());
            if(child!=null && clicklistener!=null && gestureDetector.onTouchEvent(e)){
                clicklistener.onClick(child,rv.getChildAdapterPosition(child));
            }

            return false;
        }

        @Override
        public void onTouchEvent(RecyclerView rv, MotionEvent e) {

        }

        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        /*int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }*/
        if (item.getItemId() == android.R.id.home)
        {
            finish();
        }

        return super.onOptionsItemSelected(item);
    }
}
