package com.example.lotus.garudav3.ui;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.Window;
import android.widget.Toast;

import com.example.lotus.garudav3.BookActivity;
import com.example.lotus.garudav3.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.Charset;
import java.text.DateFormatSymbols;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.Objects;

public class SplashScreen extends AppCompatActivity {

    private boolean hasGoodConnection;
    private static final String TAG = SplashScreen.class.getSimpleName();
    private String availableSeats;
//    private String arrivalCode;
//    private String departureCode;
    private int totalPax;
    private int addedPrice;
    private String departureDateStr;
    private String arrivalDateStr;
    private String dateFormatApi;
    private ArrayList<String> departureTimeList;
    private ArrayList<String> arrivalTimeList;
    private ArrayList<String> durationTimeList;
    private ArrayList<String> flightIdentification;
    private ArrayList<String> fareList;
    private String departureStationTerminal;
    private String arrivalStationTerminal;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // set activity full screen
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_splash_screen);

        departureTimeList = new ArrayList<>();
        arrivalTimeList = new ArrayList<>();
        durationTimeList = new ArrayList<>();
        flightIdentification = new ArrayList<>();
        fareList = new ArrayList<>();
        totalPax = 1;
        addedPrice = 0;
        dateFormatApi = "280218";

        String input = (2+1) + " " + 28 + " " + 2018;
        SimpleDateFormat simpledateformat = new SimpleDateFormat("EEEE");
//                        Date date = new Date(selectedYear, selectedMonth, selectedDate);
        Date date = null;
        try {
            date = new SimpleDateFormat("MM dd yyyy", Locale.ENGLISH).parse(input);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        String dayOfWeek = simpledateformat.format(date);
        String monthStr = getMonth(2+1);
        departureDateStr = "" + dayOfWeek + ", " + 28 + " " + monthStr;

        if(hasNetworkConnection()){
            new SendHttpRequest().execute();
        }else{
            finish();
        }
    }

    public String getMonth(int month) {
        return new DateFormatSymbols().getMonths()[month-1];
    }

    /**
     * Background thread with all of error handling code start from here until the end of line
     * */

    @SuppressLint("StaticFieldLeak")
    private class SendHttpRequest extends AsyncTask<Void, Void, Void> {

        //static final String REQUEST_URL = "https://garuda.client.nextflow.tech/garudatest/v1.0/Availability";
        //static final String REQUEST_URL = "https://garuda.client.nextflow.tech/garudatest/v1.0/AvailabilityWithCache";
        static final String REQUEST_URL = "https://altea-proxy.mgmt.apigateway.us/altea/availabilityFare";

        // before background thread created
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            hasGoodConnection = true;
        }

        // create background thread
        @Override
        protected Void doInBackground(Void... voids) {

            // construct URL from input parameter for url query
            Uri builtUrl = Uri.parse(REQUEST_URL).buildUpon()
                    .build();

            // create URL object from given string url
            URL url = createUrl(builtUrl.toString());
            Log.i("URL",builtUrl.toString());
            String jsonResponse = "";
            try {
                jsonResponse = makeHttpRequest(url);
                Log.i("JSON Response",jsonResponse);
            } catch (IOException e) {
                Log.e(TAG," Failed to create http request");
                hasGoodConnection = false;
                e.printStackTrace();
            }
            extractResponse(jsonResponse);
            return null;
        }

        // background thread finish
        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            if(hasGoodConnection){
                Log.d("available ", ""+availableSeats);
                Intent intent = new Intent(SplashScreen.this, BookActivity.class);
                intent.putExtra("arrivalCode","CGK");
                intent.putExtra("departureCode","SUB");
                intent.putExtra("totalPax",totalPax);
                intent.putExtra("addedPrice",addedPrice);
                intent.putExtra("departureDate",departureDateStr);
                intent.putExtra("arrivalDate",arrivalDateStr);
                intent.putExtra("availableSeats",availableSeats);
                intent.putExtra("dateFromatApi",dateFormatApi);
                intent.putStringArrayListExtra("departureTimeList",departureTimeList);
                intent.putStringArrayListExtra("arrivalTimeList",arrivalTimeList);
                intent.putStringArrayListExtra("durationTimeList",durationTimeList);
                intent.putStringArrayListExtra("flightIdentification",flightIdentification);
                intent.putStringArrayListExtra("fareList",fareList);

                intent.putExtra("departureStationTerminal",departureStationTerminal);
                intent.putExtra("arrivalStationTerminal",arrivalStationTerminal);
                startActivity(intent);
                finish();
            }else{
                Toast.makeText(SplashScreen.this, "Check your network connection and try again", Toast.LENGTH_SHORT).show();
            }
        }
    }

    // create URL object from url String
    public static URL createUrl(String urlStr){
        // return null if URL not created
        URL url = null;
        try {
            // create URL object
            url = new URL(urlStr);
        } catch (MalformedURLException e) {
            // create URL not allowed
            Log.e(TAG, "Problem building the URL ", e);
            e.printStackTrace();
        }
        return url;
    }

    // perform a network request
    public String makeHttpRequest(URL url) throws IOException {
        String jsonResponse="";

        if(url == null){
            Log.e(TAG,"There is no valid string url");
            hasGoodConnection = false;
            return null;
        }

        HttpURLConnection urlConnection = null;
        InputStream inputStream = null;

        try {
            // url.openConnection return HttpURLConnection that extend URLConnection
            urlConnection = (HttpURLConnection)url.openConnection();
            // http request method
            urlConnection.setRequestMethod("POST");
            urlConnection.setRequestProperty("Content-Type", "application/json");
            // set timeout request
            urlConnection.setConnectTimeout(10000);/* milliseconds */
            urlConnection.setReadTimeout(10000);/* milliseconds */
            urlConnection.setDoOutput(true);
            urlConnection.setDoInput(true);

            // create jsonObject for HTTP request body
            JSONObject jsonRequestBody = createJsonObject();

            if(jsonRequestBody != null){
                // send the post body
                BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(urlConnection.getOutputStream()));
                bufferedWriter.write(jsonRequestBody.toString());
                bufferedWriter.flush();

                // log response message and response code
                Log.i("Response Code ", String.valueOf(urlConnection.getResponseCode()));
                Log.i("Response Message ", urlConnection.getResponseMessage());

            }else{
                Log.e(TAG, "There is no valid JSON object");
                hasGoodConnection = false;
            }

            // if the request was succesful (response code 200)
            // then read the input stream and parse the response
            if(urlConnection.getResponseCode() == 200){
                // get the input stream from given URL
                inputStream = urlConnection.getInputStream();
                // read inputStream and return a String
                jsonResponse = readFromInputStream(inputStream);
            }
            // else, there is no input stream contain information that we want to read
            else{
                Log.e(TAG,"Error response code: "+urlConnection.getResponseCode());
                hasGoodConnection = false;
            }
        }catch (Exception e){
            hasGoodConnection = false;
            // eg there is low cellular data on smartphone
            Log.e(TAG,"Problem retrieving JSON response.",e);
        }finally {
            // cleaning the resource

            if(urlConnection != null){
                urlConnection.disconnect();
            }
            if(inputStream != null){
                // function must handle java.io.IOException here
                inputStream.close();
            }
        }

        return jsonResponse;
    }

    // read input stream from API web services and return it to human readable format
    private static String readFromInputStream(InputStream inputStream) throws IOException {
        // to store information from input stream
        StringBuilder output = new StringBuilder();
        // read input stream
        InputStreamReader inputStreamReader = new InputStreamReader(inputStream, Charset.forName("UTF-8"));
        // represent input stream to human readable text format
        BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
        String line = bufferedReader.readLine();
        while (line != null){
            output.append(line);
            line = bufferedReader.readLine();
        }
        // return information
        return  output.toString();
    }

    // create JSON object for HTTP POST request body
    public JSONObject createJsonObject(){

        try{
            JSONObject dateObject = new JSONObject();
            dateObject.put("departure_date", "280218");

            JSONArray dateArray = new JSONArray();
            dateArray.put(dateObject);

            JSONObject requestObject = new JSONObject();
            requestObject.put("availability_details", dateArray);
            requestObject.put("departure_city", "SUB");
            requestObject.put("arrival_city", "CGK");
            requestObject.put("type_of_request", "AN");

            JSONArray requestArray = new JSONArray();
            requestArray.put(requestObject);

            JSONObject segmentControlObject = new JSONObject();
            segmentControlObject.put("number_of_units", 1);
            segmentControlObject.put("quantity", 1);

            JSONArray segmentControlArray = new JSONArray();
            segmentControlArray.put(segmentControlObject);

            JSONObject jsonObject = new JSONObject();
            jsonObject.put("request_sections", requestArray);
            jsonObject.put("action_code", "44");
            jsonObject.put("business_function", "1");
            jsonObject.put("segment_control_details", segmentControlArray);

            Log.i("JSON object", jsonObject.toString());
            return jsonObject;
        }catch (JSONException e){
            Log.e(TAG,"Problem creating JSON object.",e);
            hasGoodConnection = false;
            return null;
        }
    }

    // parsing JSON response and initialize some variable from it
    public void extractResponse(String jsonResponse) {

        if(TextUtils.isEmpty(jsonResponse)){
            Log.e(TAG,"There is no valid json response");
            hasGoodConnection = false;
            return;
        }

        // If there's a problem with the way the JSON
        // is formatted, a JSONException exception object will be thrown.
        // Catch the exception so the app doesn't crash, and print the error message to the logs.
        try {
            // extract response data and initiate with variable that have been made
            JSONObject root = new JSONObject(jsonResponse);
            JSONArray singleCityPairInfos = root.getJSONArray("singleCityPairInfos");

            for(int i = 0; i < singleCityPairInfos.length();i++){
                JSONObject singleCityPairInfo = singleCityPairInfos.getJSONObject(i);
                JSONArray flightInfos = singleCityPairInfo.getJSONArray("flightInfos");
                for(int j = 0; j < flightInfos.length(); j++)
                {
                    boolean populateToListView = false;
                    JSONObject flightInfo = flightInfos.getJSONObject(j);
                    String departureTime = flightInfo.getString("departureTime");
                    String arrivalTime = flightInfo.getString("arrivalTime");
                    String durationTime = "0110";

                    try{
                        durationTime = flightInfo.getString("legDuration");
                    }catch (JSONException e){
                        Log.e(TAG, "Problem fetch legDuratin", e);
                    }
                    String identifier = flightInfo.getString("identifier");
                    String flightIdentificationNumber = flightInfo.getString("flightIdentificationNumber");
                    JSONArray productIndicators = flightInfo.getJSONArray("productIndicators");
                    for(int k=0;k<productIndicators.length();k++){
                        String code = productIndicators.getString(k);
                        Log.i("Code", code);
                        if(Objects.equals(code, "D")){
                            populateToListView = true;
                        }
                    }
                    // String departureStationTerminal = flightInfo.getString("departureStationTerminal");
                    // String arrivalStationTerminal = flightInfo.getString("arrivalStationTerminal");

                    String departureStationTerminal = "3";
                    String arrivalStationTerminal = "2";
                    String fareAmount = flightInfo.getString("fareAmount");
                    //Log.d(TAG, "harga: " + "Rp " + NumberFormat.getIntegerInstance(Locale.GERMAN).format(Integer.valueOf(fareAmount)));

                    try{
                        departureStationTerminal = flightInfo.getString("departureStationTerminal");
                    }catch (JSONException e){
                        Log.e(TAG, "Problem fetch departureStationTerminal", e);
                    }

                    try{
                        arrivalStationTerminal = flightInfo.getString("arrivalStationTerminal");
                    }catch (JSONException e){
                        Log.e(TAG, "Problem fetch arrivalStationTerminal", e);
                    }

                    this.departureStationTerminal = departureStationTerminal;
                    this.arrivalStationTerminal = arrivalStationTerminal;

                    if(populateToListView){
                        departureTimeList.add(departureTime.substring(0,2)+"."+departureTime.substring(2, 4));
                        arrivalTimeList.add(arrivalTime.substring(0,2)+"."+arrivalTime.substring(2, 4));
                        durationTimeList.add(durationTime.substring(0,2).replace("0","")+"h "+durationTime.substring(2, 4)+"m");
                        flightIdentification.add(identifier+flightIdentificationNumber);
                        fareList.add("Rp " + NumberFormat.getIntegerInstance(Locale.GERMAN).format(Integer.valueOf(fareAmount)));
                    }

                    JSONArray infoClasses = flightInfo.getJSONArray("infoOnClasses");
                    Log.d("TAG", ""+infoClasses.length());

                    for(int k = 0; k<infoClasses.length(); k++) {
                        JSONObject infoClassesObject = infoClasses.getJSONObject(k);
                        String servicesClass = infoClassesObject.getString("serviceClass");
                        if(servicesClass.contains("Y")){
                            availableSeats = infoClassesObject.getString("availabilityStatus");
                        }
                    }
                }

            }
        } catch (JSONException e) {
            // If an error is thrown when executing any of the above statements in the "try" block,
            // catch the exception here, so the app doesn't crash. Print a log message
            // with the message from the exception.
            Log.e(TAG, "Problem parsing JSON response", e);
            hasGoodConnection = false;
        }
    }

    // to check internet connection status on device eg smartphone
    boolean hasNetworkConnection(){
        ConnectivityManager connectivityManager = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivityManager != null) {
            if ( connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState() == NetworkInfo.State.CONNECTED
                    || connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).getState() == NetworkInfo.State.CONNECTED ) {
                // notify user you are online
                return true;
            }
            else if ( connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState() == NetworkInfo.State.DISCONNECTED
                    || connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).getState() == NetworkInfo.State.DISCONNECTED) {
                Toast.makeText(SplashScreen.this, "There is no network connection, activate your data cellular or connect through wifi connection", Toast.LENGTH_SHORT).show();
                return false;
            }
        }
        return false;
    }
}
