package com.example.lotus.garudav3;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.support.annotation.NonNull;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CalendarView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.Charset;
import java.text.DateFormatSymbols;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.Objects;

/**
 * Created by Lotus on 15/11/2017
 */

public class BookDetailActivity extends AppCompatActivity {

    private ArrayList<String> departureTimeList = new ArrayList<>();
    private ArrayList<String> arrivalTimeList = new ArrayList<>();
    private ArrayList<String> durationTimeList = new ArrayList<>();
    private ArrayList<String> flightIdentification = new ArrayList<>();
    private ArrayList<String> fareList = new ArrayList<>();

    Toolbar toolbar;
    Button continueBook;
    Intent intent;

    ProgressDialog pDialog;

    int totalAdult;
    int totalChildren;
    int totalInfact;
    int addedPrice;
    int totalPax;

    TextView textTotalAdult;
    TextView textTotalChildren;
    TextView textTotalInfact;
    TextView departureDateText;
    TextView arrivalDateText;

    CardView departureDate;
    CardView arrivalDate;

    Button economyButton;
    Button businessButton;
    Button firstClassButton;
    String arrivalCode;
    String departureDateStr;
    String arrivalDateStr;
    String dateFormatApi = "";
    String dateFormatApiDua = "";
    String availableSeats;

    String arrivalStationTerminal;
    String departureStationTerminal;

    boolean hasGoodConnection;
    static final String TAG = BookDetailActivity.class.getSimpleName();

    String departureCode;

    long dateDeparture;
    long dateArrival;

    long MillisecondTime, StartTime, TimeBuff, UpdateTime = 0L ;
    Handler handler;
    int Seconds, Minutes, MilliSeconds ;
    TextView timer;
    String totalTime;

    private int selectedDate,selectedMonth,selectedYear;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_book_detail);
        continueBook = (Button) findViewById(R.id.continue_book);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        //toolbar.setTextAlignment();
        setSupportActionBar(toolbar);

        getSupportActionBar().setElevation(0);
        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);

        economyButton = (Button)findViewById(R.id.class_economy);
        businessButton = (Button)findViewById(R.id.class_business);
        firstClassButton = (Button)findViewById(R.id.class_first_class);
        departureDate = (CardView) findViewById(R.id.departure_date);
        arrivalDate = (CardView) findViewById(R.id.arrival_date);

        getSupportActionBar().setElevation(0);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        textTotalAdult = (TextView)findViewById(R.id.total_adult);
        textTotalChildren = (TextView)findViewById(R.id.total_children);
        textTotalInfact = (TextView)findViewById(R.id.total_infact);
        departureDateText = (TextView)findViewById(R.id.departure_date_text);
        arrivalDateText = (TextView)findViewById(R.id.arrival_date_text);

        totalAdult = 0;
        totalChildren = 0;
        totalInfact = 0;
        addedPrice = 0;

        totalAdult++;
        updateNumber();

        handler = new Handler() ;

        Intent i = getIntent();
        arrivalCode = i.getStringExtra("arrivalCode");
        departureCode = i.getStringExtra("departureCode");


        TextView departurePort = (TextView)findViewById(R.id.departure_port);
        departurePort.setText(departureCode);

        if(i.getStringExtra("departureDate") != null) {
            departureDateStr = i.getStringExtra("departureDate");
            Log.e("selected", departureDateStr);

            departureDateText.setText(departureDateStr.split(", ")[1]);
        }

        if(i.getStringExtra("arrivalDate") != null) {
            arrivalDateStr = i.getStringExtra("arrivalDate");
            Log.e("TAG", "arrivaldateStr: " + arrivalDateStr);

            arrivalDateText.setText(arrivalDateStr.split(", ")[1]);
        }

        if(i.getStringExtra("dateFromWeb") != null) {
            dateFormatApi = i.getStringExtra("dateFromWeb");
            Log.e("selected", dateFormatApi);

            departureDateText.setText(cekTanggal(dateFormatApi));
        }

        TextView arrival = (TextView)findViewById(R.id.arrival_port);
        arrival.setText(arrivalCode);

        businessButton.setEnabled(false);
        firstClassButton.setEnabled(false);

        economyButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                economyButton.setBackgroundResource(R.drawable.layout_button_blue_left);
                businessButton.setBackgroundResource(R.drawable.layout_button_white_mid);
                firstClassButton.setBackgroundResource(R.drawable.layout_button_white_right);
                addedPrice = 0;
                economyButton.setTextColor(Color.parseColor("#ffffff"));
                businessButton.setTextColor(Color.parseColor("#8d9298"));
                firstClassButton.setTextColor(Color.parseColor("#8d9298"));
            }
        });

        businessButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                businessButton.setBackgroundResource(R.drawable.layout_button_blue_left);
                economyButton.setBackgroundResource(R.drawable.layout_button_white_left);
                firstClassButton.setBackgroundResource(R.drawable.layout_button_white_right);
                economyButton.setTextColor(Color.parseColor("#8d9298"));
                firstClassButton.setTextColor(Color.parseColor("#8d9298"));
                businessButton.setTextColor(Color.parseColor("#ffffff"));
                addedPrice = 150;
            }
        });

        firstClassButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                firstClassButton.setBackgroundResource(R.drawable.layout_button_blue_left);
                economyButton.setBackgroundResource(R.drawable.layout_button_white_left);
                businessButton.setBackgroundResource(R.drawable.layout_button_white_mid);
                economyButton.setTextColor(Color.parseColor("#8d9298"));
                businessButton.setTextColor(Color.parseColor("#8d9298"));
                firstClassButton.setTextColor(Color.parseColor("#ffffff"));
                addedPrice = 300;
            }
        });


        continueBook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(hasNetworkConnection()){
                    if(dateFormatApi.isEmpty() || dateFormatApiDua.isEmpty())
                        Toast.makeText(getApplicationContext(), "Please input the flight date", Toast.LENGTH_SHORT).show();
                    else
                        new SendHttpRequest().execute();
                }else{
                    // eg smartphone is in airplane mode
                    Toast.makeText(BookDetailActivity.this, "There is no network connection, activate your data cellular or connect through wifi connection", Toast.LENGTH_SHORT).show();
                }

            }
        });

        departureDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // custom dialog
                final Dialog dialog = new Dialog(BookDetailActivity.this);

                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.date_picker);
                dialog.setTitle("");
                Window window = dialog.getWindow();
                window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
                WindowManager.LayoutParams wlp = window.getAttributes();

                wlp.gravity = Gravity.BOTTOM;
//                wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
                window.setDimAmount(0.5f);
                window.setAttributes(wlp);

                CalendarView datePicker = (CalendarView) dialog.findViewById(R.id.date_picker);
                datePicker.setMinDate(System.currentTimeMillis()+86400000);
                Calendar calendar = Calendar.getInstance();
                calendar.setTimeInMillis(System.currentTimeMillis());
                selectedDate=calendar.get(Calendar.DAY_OF_MONTH);
                selectedMonth=calendar.get(Calendar.MONTH);
                selectedYear=calendar.get(Calendar.YEAR);
                datePicker.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {
                    @Override
                    public void onSelectedDayChange(@NonNull CalendarView calendarView, int year, int month, int dayOfMonth) {
                        dateFormatApi = "";
                        String dayForApi = dayOfMonth < 10 ? dateFormatApi + "0" + dayOfMonth : dateFormatApi + ""+dayOfMonth;
                        String monthForApi = month < 10 ? dateFormatApi + "0" + (month+1) : dateFormatApi + ""+ (month+1);
                        String yearForApi = dateFormatApi + String.valueOf(year).substring(2,4);

                        dateFormatApi = dayForApi + monthForApi + yearForApi;
                        Log.e("dateFormatApi", ""+ dateFormatApi);

                        if(selectedDate ==dayOfMonth && selectedMonth==month && selectedYear==year) {
                            dialog.dismiss();
                        }else {
                            if(selectedDate !=dayOfMonth){
                                dialog.dismiss();
                            }else {
                                if(selectedMonth !=month){
                                    dialog.dismiss();
                                }
                            }
                        }
                        selectedDate=dayOfMonth;
                        selectedMonth=month;
                        selectedYear=year;

                        String input = (selectedMonth+1) + " " + selectedDate + " " + selectedYear;
//                        Log.i("input",input);

                        SimpleDateFormat simpledateformat = new SimpleDateFormat("EEEE");
//                        Date date = new Date(selectedYear, selectedMonth, selectedDate);
                        Date date = null;
                        try {
                            date = new SimpleDateFormat("MM dd yyyy", Locale.ENGLISH).parse(input);
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                        dateDeparture = date.getTime();
                        String dayOfWeek = simpledateformat.format(date);
                        String monthStr = getMonth(month+1);
                        departureDateStr = "" + dayOfWeek + ", " + selectedDate + " " + monthStr;

                        departureDateText.setText(departureDateStr.split(", ")[1]);
                        //Toast.makeText(BookDetailActivity.this, "" + dayOfWeek + ", " + selectedDate + " ", Toast.LENGTH_SHORT).show();
                    }

                });
                dialog.show();
            }
        });

        arrivalDate.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                // custom dialog
                final Dialog dialog = new Dialog(BookDetailActivity.this);

                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.date_picker);
                dialog.setTitle("");
                Window window = dialog.getWindow();
                window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
                WindowManager.LayoutParams wlp = window.getAttributes();

                wlp.gravity = Gravity.BOTTOM;
//                wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
                window.setDimAmount(0.5f);
                window.setAttributes(wlp);


                CalendarView datePicker = dialog.findViewById(R.id.date_picker);
                Calendar calendar = Calendar.getInstance();
//                datePicker.setMinDate(System.currentTimeMillis()+(86400000*2));
                datePicker.setMinDate(dateDeparture);
                calendar.setTimeInMillis(System.currentTimeMillis());
                selectedDate=calendar.get(Calendar.DAY_OF_MONTH);
                selectedMonth=calendar.get(Calendar.MONTH);
                selectedYear=calendar.get(Calendar.YEAR);
                /*datePicker.init(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH), new DatePicker.OnDateChangedListener() {

                    @Override
                    public void onDateChanged(DatePicker datePicker, int year, int month, int dayOfMonth) {*/
                datePicker.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {
                    @Override
                    public void onSelectedDayChange(@NonNull CalendarView calendarView, int year, int month, int dayOfMonth) {
                        dateFormatApiDua = "";
                        String dayForApi = dayOfMonth < 10 ? dateFormatApiDua + "0" + dayOfMonth : dateFormatApiDua + ""+dayOfMonth;
                        String monthForApi = month < 10 ? dateFormatApiDua + "0" + (month+1) : dateFormatApiDua + ""+(month+1);
                        String yearForApi = dateFormatApiDua + String.valueOf(year).substring(2,4);

                        dateFormatApiDua = dayForApi + monthForApi + yearForApi;
                        Log.e("dateFormatApiDua", ""+ dateFormatApiDua);

                        if(selectedDate ==dayOfMonth && selectedMonth==month && selectedYear==year) {
                            dialog.dismiss();
                        }else {
                            if(selectedDate !=dayOfMonth){
                                dialog.dismiss();
                            }else {
                                if(selectedMonth !=month){
                                    dialog.dismiss();
                                }
                            }
                        }
                        selectedDate=dayOfMonth;
                        selectedMonth=month;
                        selectedYear=year;

                        String input = (selectedMonth+1) + " " + selectedDate + " " + selectedYear;
//                        Log.i("input",input);

                        SimpleDateFormat simpledateformat = new SimpleDateFormat("EEEE");
                        Date date = null;
                        try {
                            date = new SimpleDateFormat("MM dd yyyy", Locale.ENGLISH).parse(input);
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                        dateArrival = date.getTime();
                        String dayOfWeek = simpledateformat.format(date);
                        String monthStr = getMonth(month+1);
                        arrivalDateStr = "" + dayOfWeek + ", " + selectedDate + " " + monthStr;

                        arrivalDateText.setText(arrivalDateStr.split(", ")[1]);


                        long compare = dateDeparture-System.currentTimeMillis();
                        Log.i("curren ","" + System.currentTimeMillis());
                        Log.i("depart ","" + dateDeparture);
                        Log.i("arrive ","" + dateArrival);
                        Log.i("arrive ","" + departureDateStr);
                        Log.i("arrive ","" + arrivalDateStr);
                        Log.i("current - departure ", ""+compare);
                        //Toast.makeText(BookDetailActivity.this, "" + dayOfWeek + ", " + selectedDate + " ", Toast.LENGTH_SHORT).show();
                    }

                });
                dialog.show();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == android.R.id.home)
        {
            finish();
        }

        return super.onOptionsItemSelected(item);
    }


    public void upAdult(View v){
        totalAdult++;
        updateNumber();
    }
    public void downAdult(View v){
        if(totalAdult > 0){
            totalAdult--;
        }
        updateNumber();
    }
    public void upChildren(View v){
        totalChildren++;
        updateNumber();
    }
    public void downChildren(View v){
        if(totalChildren > 0){
            totalChildren--;
        }
        updateNumber();
    }
    public void upInfact(View v){
        totalInfact++;
        updateNumber();
    }
    public void downInfact(View v){
        if(totalInfact > 0){
            totalInfact--;
        }
        updateNumber();
    }

    void updateNumber(){

        totalPax = totalAdult + totalChildren + totalInfact;

        String adult = "" + totalAdult;
        String children = "" + totalChildren;
        String infact = "" + totalInfact;


        textTotalAdult.setText(adult);
        textTotalChildren.setText(children);
        textTotalInfact.setText(infact);
    }


    public String getMonth(int month) {
        return new DateFormatSymbols().getMonths()[month-1];
    }


    /**
    * Background thread with all of error handling code start from here until the end of line
    * */

    @SuppressLint("StaticFieldLeak")
    private class SendHttpRequest extends AsyncTask<Void, Void, Void>{

        //static final String REQUEST_URL = "https://garuda.client.nextflow.tech/garudatest/v1.0/Availability";
        //static final String REQUEST_URL = "https://garuda.client.nextflow.tech/garudatest/v1.0/AvailabilityWithCache";
        static final String REQUEST_URL = "https://altea-proxy.mgmt.apigateway.us/altea/availabilityFare";

        // before background thread created
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            hasGoodConnection = true;
            pDialog = new ProgressDialog(BookDetailActivity.this,R.style.full_screen_dialog){
                @Override
                protected void onCreate(Bundle savedInstanceState) {
                    super.onCreate(savedInstanceState);
                    setContentView(R.layout.fill_dialog);
                    getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT,
                            ViewGroup.LayoutParams.MATCH_PARENT);

                    /*ImageView imageView = (ImageView) getWindow().findViewById(R.id.progressBar1);
                    Glide.with(BookDetailActivity.this).load(R.raw.loading9).into(imageView);*/

                    timer = (TextView) getWindow().findViewById(R.id.timer);
                    StartTime = SystemClock.uptimeMillis();
                    handler.postDelayed(runnable, 0);
                }
            };

            pDialog.setCancelable(false);
            pDialog.show();
        }

        // create background thread
        @Override
        protected Void doInBackground(Void... voids) {

            // construct URL from input parameter for url query
            Uri builtUrl = Uri.parse(REQUEST_URL).buildUpon()
                    .build();

            // create URL object from given string url
            URL url = createUrl(builtUrl.toString());
            Log.i("URL",builtUrl.toString());
            String jsonResponse = "";
            try {
                jsonResponse = makeHttpRequest(url);
                Log.i("jsonResponse",jsonResponse);
            } catch (IOException e) {
                Log.e(TAG," Failed to create http request");
                hasGoodConnection = false;
                e.printStackTrace();
            }
            extractResponse(jsonResponse);
            return null;
        }

        // background thread finish
        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            if (pDialog.isShowing()) {
                pDialog.dismiss();
                handler.removeCallbacks(runnable);
            }

            if(hasGoodConnection){
                intent = new Intent(BookDetailActivity.this, SearchResultRoundFirst.class);
                intent.putExtra("arrivalCode",arrivalCode);
                intent.putExtra("departureCode",departureCode);
                intent.putExtra("totalPax",totalPax);
                intent.putExtra("addedPrice",addedPrice);
                intent.putExtra("departureDate",departureDateStr);
                intent.putExtra("arrivalDate",arrivalDateStr);
                intent.putExtra("availableSeats",availableSeats);
                intent.putExtra("dateFromatApi",dateFormatApi);
                intent.putExtra("dateFromatApiDua",dateFormatApiDua);
                intent.putExtra("totalTime",totalTime);
                intent.putStringArrayListExtra("departureTimeList",departureTimeList);
                intent.putStringArrayListExtra("arrivalTimeList",arrivalTimeList);
                intent.putStringArrayListExtra("durationTimeList",durationTimeList);
                intent.putStringArrayListExtra("flightIdentification",flightIdentification);
                intent.putStringArrayListExtra("fareList",fareList);

                intent.putExtra("departureStationTerminal",departureStationTerminal);
                intent.putExtra("arrivalStationTerminal",arrivalStationTerminal);
                startActivity(intent);
            }else{
                Toast.makeText(BookDetailActivity.this, "Check your network connection and try again", Toast.LENGTH_SHORT).show();
            }
        }
    }

    // create URL object from url String
    public static URL createUrl(String urlStr){
        // return null if URL not created
        URL url = null;
        try {
            // create URL object
            url = new URL(urlStr);
        } catch (MalformedURLException e) {
            // create URL not allowed
            Log.e(TAG, "Problem building the URL ", e);
            e.printStackTrace();
        }
        return url;
    }

    // perform a network request
    public String makeHttpRequest(URL url) throws IOException {
        String jsonResponse="";

        if(url == null){
            Log.e(TAG,"There is no valid string url");
            hasGoodConnection = false;
            return null;
        }

        HttpURLConnection urlConnection = null;
        InputStream inputStream = null;

        try {
            // url.openConnection return HttpURLConnection that extend URLConnection
            urlConnection = (HttpURLConnection)url.openConnection();
            // http request method
            urlConnection.setRequestMethod("POST");
            urlConnection.setRequestProperty("Content-Type", "application/json");
            // set timeout request
            urlConnection.setConnectTimeout(10000);/* milliseconds */
            urlConnection.setReadTimeout(10000);/* milliseconds */
            urlConnection.setDoOutput(true);
            urlConnection.setDoInput(true);

            // create jsonObject for HTTP request body
            JSONObject jsonRequestBody = createJsonObject();

            if(jsonRequestBody != null){
                // send the post body
                BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(urlConnection.getOutputStream()));
                bufferedWriter.write(jsonRequestBody.toString());
                bufferedWriter.flush();

                // log response message and response code
                Log.i("Response Code ", String.valueOf(urlConnection.getResponseCode()));
                Log.i("Response Message ", urlConnection.getResponseMessage());

            }else{
                Log.e(TAG, "There is no valid JSON object");
                hasGoodConnection = false;
            }

            // if the request was succesful (response code 200)
            // then read the input stream and parse the response
            if(urlConnection.getResponseCode() == 200){
                // get the input stream from given URL
                inputStream = urlConnection.getInputStream();
                // read inputStream and return a String
                jsonResponse = readFromInputStream(inputStream);
            }
            // else, there is no input stream contain information that we want to read
            else{
                Log.e(TAG,"Error response code: "+urlConnection.getResponseCode());
                hasGoodConnection = false;
            }
        }catch (Exception e){
            hasGoodConnection = false;
            // eg there is low cellular data on smartphone
            Log.e(TAG,"Problem retrieving JSON response.",e);
        }finally {
            // cleaning the resource

            if(urlConnection != null){
                urlConnection.disconnect();
            }
            if(inputStream != null){
                // function must handle java.io.IOException here
                inputStream.close();
            }
        }

        return jsonResponse;
    }

    // read input stream from API web services and return it to human readable format
    private static String readFromInputStream(InputStream inputStream) throws IOException {
        // to store information from input stream
        StringBuilder output = new StringBuilder();
        // read input stream
        InputStreamReader inputStreamReader = new InputStreamReader(inputStream, Charset.forName("UTF-8"));
        // represent input stream to human readable text format
        BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
        String line = bufferedReader.readLine();
        while (line != null){
            output.append(line);
            line = bufferedReader.readLine();
        }
        // return information
        return  output.toString();
    }

    // create JSON object for HTTP POST request body
    public JSONObject createJsonObject(){

        try{
            JSONObject dateObject = new JSONObject();
            dateObject.put("departure_date", dateFormatApi);

            JSONArray dateArray = new JSONArray();
            dateArray.put(dateObject);

            JSONObject requestObject = new JSONObject();
            requestObject.put("departure_city", departureCode);
            requestObject.put("arrival_city", arrivalCode);
            requestObject.put("type_of_request", "AN");
            requestObject.put("availability_details", dateArray);

            JSONArray requestArray = new JSONArray();
            requestArray.put(requestObject);

            JSONObject segmentControlObject = new JSONObject();
            segmentControlObject.put("number_of_units", 1);
            segmentControlObject.put("quantity", 1);

            JSONArray segmentControlArray = new JSONArray();
            segmentControlArray.put(segmentControlObject);

            JSONObject jsonObject = new JSONObject();
            jsonObject.put("action_code", "44");
            jsonObject.put("business_function", "1");
            jsonObject.put("request_sections", requestArray);
            jsonObject.put("segment_control_details", segmentControlArray);

            Log.i("JSON object", jsonObject.toString());
            return jsonObject;
        }catch (JSONException e){
            Log.e(TAG,"Problem creating JSON object.",e);
            hasGoodConnection = false;
            return null;
        }
    }

    // parsing JSON response and initialize some variable from it
    public void extractResponse(String jsonResponse) {

        if(TextUtils.isEmpty(jsonResponse)){
            Log.e(TAG,"There is no valid json response");
            hasGoodConnection = false;
            return;
        }

        // If there's a problem with the way the JSON
        // is formatted, a JSONException exception object will be thrown.
        // Catch the exception so the app doesn't crash, and print the error message to the logs.
        try {
            // extract response data and initiate with variable that have been made
            JSONObject root = new JSONObject(jsonResponse);
            JSONArray singleCityPairInfos = root.getJSONArray("singleCityPairInfos");

            for(int i = 0; i < singleCityPairInfos.length();i++){
                JSONObject singleCityPairInfo = singleCityPairInfos.getJSONObject(i);
                JSONArray flightInfos = singleCityPairInfo.getJSONArray("flightInfos");
                for(int j = 0; j < flightInfos.length(); j++)
                {
                    boolean populateToListView = false;
                    JSONObject flightInfo = flightInfos.getJSONObject(j);
                    String departureTime = flightInfo.getString("departureTime");
                    String arrivalTime = flightInfo.getString("arrivalTime");
                    String durationTime = "0110";
                    try{
                        durationTime = flightInfo.getString("legDuration");
                    }catch (JSONException e){
                        Log.e(TAG, "Problem fetch legDuratin", e);
                    }
                    String identifier = flightInfo.getString("identifier");
                    String flightIdentificationNumber = flightInfo.getString("flightIdentificationNumber");
                    JSONArray productIndicators = flightInfo.getJSONArray("productIndicators");
                    for(int k=0;k<productIndicators.length();k++){
                        String code = productIndicators.getString(k);
                        Log.i("Code", code);
                        if(Objects.equals(code, "D")){
                            populateToListView = true;
                        }
                    }
//                    String departureStationTerminal = flightInfo.getString("departureStationTerminal");
//                    String arrivalStationTerminal = flightInfo.getString("arrivalStationTerminal");

                    String departureStationTerminal = "3";
                    String arrivalStationTerminal = "2";
                    String fareAmount = flightInfo.getString("fareAmount");

                    try{
                        departureStationTerminal = flightInfo.getString("departureStationTerminal");
                    }catch (JSONException e){
                        Log.e(TAG, "Problem fetch departureStationTerminal", e);
                    }

                    try{
                        arrivalStationTerminal = flightInfo.getString("arrivalStationTerminal");
                    }catch (JSONException e){
                        Log.e(TAG, "Problem fetch arrivalStationTerminal", e);
                    }

                    this.departureStationTerminal = departureStationTerminal;
                    this.arrivalStationTerminal = arrivalStationTerminal;

                    if(populateToListView){
                        departureTimeList.add(departureTime.substring(0,2)+"."+departureTime.substring(2, 4));
                        arrivalTimeList.add(arrivalTime.substring(0,2)+"."+arrivalTime.substring(2, 4));
                        durationTimeList.add(durationTime.substring(0,2).replace("0","")+"h "+durationTime.substring(2, 4)+"m");
                        flightIdentification.add(identifier+flightIdentificationNumber);
                        fareList.add("Rp " + NumberFormat.getIntegerInstance(Locale.GERMAN).format(Integer.valueOf(fareAmount)));
                    }

                    JSONArray infoClasses = flightInfo.getJSONArray("infoOnClasses");

                    for(int k = 0; k<infoClasses.length(); k++) {
                        JSONObject infoClassesObject = infoClasses.getJSONObject(k);
                        String servicesClass = infoClassesObject.getString("serviceClass");
                        if(servicesClass.contains("Y")){
                            availableSeats = infoClassesObject.getString("availabilityStatus");
                        }
                    }
                }

            }
        } catch (JSONException e) {
            // If an error is thrown when executing any of the above statements in the "try" block,
            // catch the exception here, so the app doesn't crash. Print a log message
            // with the message from the exception.
            Log.e(TAG, "Problem parsing JSON response", e);
            hasGoodConnection = false;
        }
    }

    // to check internet connection status on device eg smartphone
    boolean hasNetworkConnection(){
        ConnectivityManager connectivityManager = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivityManager != null) {
            if ( connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState() == NetworkInfo.State.CONNECTED
                    || connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).getState() == NetworkInfo.State.CONNECTED ) {
                // notify user you are online
                return true;
            }
            else if ( connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState() == NetworkInfo.State.DISCONNECTED
                    || connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).getState() == NetworkInfo.State.DISCONNECTED) {
                // notify user you are not online
                return false;
            }
        }
        return false;
    }

    public String cekTanggal(String tanggalBulan){
        String tanggal = tanggalBulan.substring(0,2);
        String bulan = tanggalBulan.substring(2,4);

        switch (bulan) {
            case "01":
                bulan = "Januari";
                break;
            case "02":
                bulan = "Februari";
                break;
            case "03":
                bulan = "Maret";
                break;
            case "04":
                bulan = "April";
                break;
            case "05":
                bulan = "Mei";
                break;
            case "06":
                bulan = "Juni";
                break;
            case "07":
                bulan = "Juli";
                break;
            case "08":
                bulan = "Agustus";
                break;
            case "09":
                bulan = "September";
                break;
            case "10":
                bulan = "Oktober";
                break;
            case "11":
                bulan = "November";
                break;
            case "12":
                bulan = "Desember";
                break;
        }

        return tanggal + " " + bulan;
    }

    public Runnable runnable = new Runnable() {
        public void run() {
            MillisecondTime = SystemClock.uptimeMillis() - StartTime;

            UpdateTime = TimeBuff + MillisecondTime;
            Seconds = (int) (UpdateTime / 1000);
            Minutes = Seconds / 60;
            Seconds = Seconds % 60;
            MilliSeconds = (int) (UpdateTime % 1000);

            totalTime = "" + String.format("%01d", Seconds) + "." + String.format("%03d", MilliSeconds);
            timer.setText(totalTime);

            handler.postDelayed(this, 0);
        }
    };
}
