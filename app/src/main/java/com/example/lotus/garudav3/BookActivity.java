package com.example.lotus.garudav3;

import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.example.lotus.garudav3.adapter.PagerAdapter;
import com.example.lotus.garudav3.fragment.SingleTripFragment;


public class BookActivity extends AppCompatActivity {

    private Toolbar toolbar;
    public TabLayout tabLayout;
    private ViewPager viewPager;
    public PagerAdapter adapter;

    private static final String LAUNCH_FROM_URL = "com.example.lotus.garudav5";
    public String msgFromBrowserUrl;

    public String date="";
    public String origin="CGK";
    public String departure="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_book);

        Intent in = getIntent();
        Uri data = in.getData();
        if(data!= null && !Uri.EMPTY.equals(data) && data.toString().contains("?")) {
            date = data.getQueryParameter("date");
            origin = data.getQueryParameter("origin");
            departure = data.getQueryParameter("departure");
            Log.d("json :- ", data.toString() + " " + date + " " + origin + " " + departure);
        }

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        findViewById(R.id.appBarLayout).bringToFront();

        getSupportActionBar().setElevation(0);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tab_layout);
        tabLayout.addTab(tabLayout.newTab().setText("Single Trip"));
        tabLayout.addTab(tabLayout.newTab().setText("Round Trip"));
//        tabLayout.addTab(tabLayout.newTab().setText("Multi City"));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        tabLayout.setSelectedTabIndicatorColor(Color.parseColor("#ffffff"));

        Intent intent = getIntent();
        if(intent != null && intent.hasExtra("msg_from_browser")){
                msgFromBrowserUrl = intent.getStringExtra("msg_from_browser");
                Log.d("TAG json", msgFromBrowserUrl);
        }else{
            msgFromBrowserUrl = "no intent";
            Log.d("TAG json", msgFromBrowserUrl);
        }


        final ViewPager viewPager = (ViewPager) findViewById(R.id.pager);
        final PagerAdapter adapter = new PagerAdapter(getSupportFragmentManager(), tabLayout.getTabCount());
        viewPager.setAdapter(adapter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        /*int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }*/
        if (item.getItemId() == android.R.id.home)
        {
            finish();
        }

        return super.onOptionsItemSelected(item);
    }
}