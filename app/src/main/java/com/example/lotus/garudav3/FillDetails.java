package com.example.lotus.garudav3;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.text.NumberFormat;
import java.util.Locale;

/**
 * Created by Lotus on 20/11/2017.
 */

public class FillDetails extends AppCompatActivity {

    Toolbar toolbar;
    Button continueBook;
    Intent intent;
    String arrivalCode;
    String departureDate;
    String arrivalDate;

    String departureCode;
    String timeFlight;
    int totalPax;
    int addedPrice;
    int priceInt = 0;
    int totalTax = 0;

    CheckBox travelInsurance;

    TextView priceTotal;
    TextView time;
    TextView duration;
    TextView flightIdentifier;
    TextView timerTv;
    String totalTime;

    String arrivalStationTerminal;
    String departureStationTerminal;
    String totalPaxStr;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fill_book_detail);

        continueBook = (Button) findViewById(R.id.continue_book);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setElevation(0);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        travelInsurance = (CheckBox) findViewById(R.id.travel_insurance);
        time = (TextView) findViewById(R.id.time);
        duration = (TextView) findViewById(R.id.duration);

        Intent i = getIntent();
        departureCode = i.getStringExtra("departureCode");
        arrivalCode = i.getStringExtra("arrivalCode");
        totalPax = i.getIntExtra("totalPax",0);
        departureDate = i.getStringExtra("departureDate");
        arrivalDate = i.getStringExtra("arrivalDate");
        totalTax = i.getIntExtra("totalTax", 0);
        totalTime = i.getStringExtra("totalTime");
        final String price = i.getStringExtra("price");
        final String timeFlight = i.getStringExtra("timeFlight");
        final String durationFlight = i.getStringExtra("durationFlight");
        final String flightIdentification = i.getStringExtra("flightIdentification");
        final String totalHarga = i.getStringExtra("totalHarga");

        departureStationTerminal = i.getStringExtra("departureStationTerminal");
        arrivalStationTerminal = i.getStringExtra("arrivalStationTerminal");
        TextView departureTerminal = (TextView)findViewById(R.id.departure);
        TextView arrivalTerminal = (TextView)findViewById(R.id.arrival);
        departureTerminal.setText("T. "+departureStationTerminal);
        arrivalTerminal.setText("T. "+arrivalStationTerminal);

        //priceInt = Integer.parseInt(price);
        Log.d("TAG", totalTax+"");

        TextView arrival = (TextView)findViewById(R.id.arrival_port);
        TextView summary = (TextView)findViewById(R.id.summary_book);
        TextView priceStr = (TextView)findViewById(R.id.price);
        TextView priceReal = (TextView)findViewById(R.id.price_real);
        timerTv = (TextView) findViewById(R.id.timerTv);

        priceTotal = (TextView)findViewById(R.id.price_total);
        flightIdentifier = (TextView)findViewById(R.id.point);

        timerTv.setText("Last processed time: " + totalTime + "s");
        arrival.setText(arrivalCode);
        totalPaxStr = departureDate.split(", ")[1].split(" ")[0] + " " +
                departureDate.split(", ")[1].split(" ")[1].substring(0,3) + ", " + totalPax + " pax";
        summary.setText(totalPaxStr);
        priceStr.setText(price);
        priceReal.setText("Rp " + NumberFormat.getIntegerInstance(Locale.GERMAN).format(Integer.valueOf(totalHarga)*totalPax));
        priceTotal.setText("Rp " + NumberFormat.getIntegerInstance(Locale.GERMAN).format(Integer.valueOf(totalHarga)*totalPax));
        flightIdentifier.setText(flightIdentification);
        time.setText(timeFlight);
        duration.setText(durationFlight);

        priceInt = Integer.parseInt(totalHarga);

        travelInsurance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //is chkIos checked?
                if (((CheckBox) v).isChecked()) {
                    priceInt += 26000;
                    priceTotal.setText("Rp " + NumberFormat.getIntegerInstance(Locale.GERMAN).format(priceInt*totalPax));
                }
                else {
                    priceInt -= 26000;
                    priceTotal.setText("Rp " + NumberFormat.getIntegerInstance(Locale.GERMAN).format(priceInt*totalPax));
                }
            }
        });

        TextView departurePort = (TextView)findViewById(R.id.departure_port);
        departurePort.setText(departureCode);
        TextView bagagge = (TextView)findViewById(R.id.bagagge);
        String bagaggeStr = "CGK - " + arrivalCode;
        bagagge.setText(bagaggeStr);

        continueBook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String priceFix = priceTotal.getText().toString();

                if(!TextUtils.isEmpty(arrivalDate)) {
                    intent = new Intent(FillDetails.this, ReviewBooking.class);
                    intent.putExtra("departureCode", departureCode);
                    intent.putExtra("arrivalCode", arrivalCode);
                    intent.putExtra("totalPax", totalPax);
                    intent.putExtra("departureDate", departureDate);
                    intent.putExtra("arrivalDate", arrivalDate);
                    intent.putExtra("price", priceFix.substring(3, priceFix.length() - 4));
                    intent.putExtra("priceInt", priceInt);
                    intent.putExtra("departureStationTerminal",departureStationTerminal);
                    intent.putExtra("arrivalStationTerminal",arrivalStationTerminal);
                    startActivity(intent);
                }
                else{
                    intent = new Intent(FillDetails.this, ReviewBookingSingle.class);
                    intent.putExtra("departureCode", departureCode);
                    intent.putExtra("arrivalCode", arrivalCode);
                    intent.putExtra("totalPax", totalPax);
                    intent.putExtra("departureDate", departureDate);
                    intent.putExtra("arrivalDate", arrivalDate);
                    intent.putExtra("price", price);
                    intent.putExtra("priceInt", String.valueOf(priceInt));
                    intent.putExtra("totalTax",totalTax);
                    Log.d("TAG", "Price: "+price);
                    intent.putExtra("departureStationTerminal",departureStationTerminal);
                    intent.putExtra("arrivalStationTerminal",arrivalStationTerminal);
                    intent.putExtra("totalPaxStr",totalPaxStr);
                    startActivity(intent);
                }
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home)
        {
            finish();
        }

        return super.onOptionsItemSelected(item);
    }
}
