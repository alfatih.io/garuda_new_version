package com.example.lotus.garudav3.adapter;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.example.lotus.garudav3.model.Destination;
import com.example.lotus.garudav3.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by al-fatih on 27/11/17
 */

public class DestinationAdapter extends RecyclerView.Adapter<DestinationAdapter.MyViewHolder> implements Filterable {

    private List<Destination> destinationList;
    public List<Destination> destinationListFiltered;

    public class MyViewHolder extends RecyclerView.ViewHolder{
        public TextView city, airport;

        MyViewHolder(View itemView) {
            super(itemView);
            city = itemView.findViewById(R.id.city);
            airport = itemView.findViewById(R.id.airport);
        }
    }

    public DestinationAdapter(List<Destination> destinationList){
        this.destinationList = destinationList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.destination_list, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Destination destination = destinationList.get(position);
        holder.city.setText(destination.getCity());
        holder.airport.setText(destination.getAirport());
    }

    @Override
    public int getItemCount() {
        return destinationList.size();
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                Log.d("cobaChar ", charString);
                if (charString.isEmpty()) {
                    destinationListFiltered = destinationList;
                } else {
                    List<Destination> filteredList = new ArrayList<>();
                    for (Destination row : destinationList) {
                        Log.d("cobaCharSebelum", charString);
                        //Log.d("cobaCharSebelum", row.getCity().toLowerCase());
                        if (row.getCity().toLowerCase().contains(charString)){; //|| row.getAirport().contains(charSequence)) {
                            Log.d("cobaCharSetelah ", charString);
                            filteredList.add(row);
                        }
                    }

                    destinationListFiltered = filteredList;
                }


                FilterResults filterResults = new FilterResults();
                filterResults.values = destinationListFiltered;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                destinationListFiltered = (ArrayList<Destination>) filterResults.values;
                Log.d("coba size", "size: " + destinationListFiltered.size());
                notifyDataSetChanged();
            }
        };
    }
}
