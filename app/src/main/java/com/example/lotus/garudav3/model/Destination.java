package com.example.lotus.garudav3.model;

/**
 * Created by al-fatih on 27/11/17
 */

public class Destination {

    private String city;
    private String airport;
    private int logo;

    public Destination(String city, String airport){
        this.city = city;
        this.airport = airport;
    }

    public Destination(String city, String airport, int logo){
        this.city = city;
        this.airport = airport;
        this.logo = logo;
    }

    public String getCity(){
        return this.city;
    }

    public String getAirport(){
        return this.airport;
    }

    public int getLogo(){
        return this.logo;
    }
}
