package com.example.lotus.garudav3.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.example.lotus.garudav3.SearchResult;
import com.example.lotus.garudav3.SearchResultRoundFirst;
import com.example.lotus.garudav3.fragment.RoundTripFragment;
import com.example.lotus.garudav3.fragment.SingleTripFragment;

/**
 * Created by Lotus on 15/11/2017
 */

public class PagerAdapter extends FragmentPagerAdapter {
    private int mNumOfTabs;

    public PagerAdapter(FragmentManager fm, int NumOfTabs) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;
    }

    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
                return new SearchResult();
            case 1:
                return new SearchResultRoundFirst();
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }
}
